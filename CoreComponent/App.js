import { StyleSheet, Text, View, ScrollView,TextInput, Image, Button } from 'react-native';

export default function App() {
  return (
    <View  style={styles.container} >
      <ScrollView>
        <Text >Hello Everyone!</Text>
         <View>
        <Text>I am Tenzin Tshomo!</Text>
        <Image style={styles.image}
        source={{
          uri: 'https://picsum.photos/64/64',
        }} />
        </View>
        <TextInput   
        defaultvalues= "you can type here" />
        <Button
           onPress={() =>  {
          alert('You tapped the button!');
        }} 
      
           title="Press Me" />
         <Text numberOfLines={1}>A quick brown fox jumps over the lazy dog</Text>  
        </ScrollView> 
        
    </View>  
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    marginTop: 50,
    marginRight: 30,
    justifyContent: 'center',
  },
    text: {
      flex: 1,
      justifyContent: 'center',
    },
  image: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    
  },
  
  });
