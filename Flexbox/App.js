import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (

  <View style={styles.container}>
      <View style={styles.square} />
      <View style={styles.square} />
      <View style={styles.square} />
  </View>
  );}
const styles = StyleSheet.create({ 
  container:{
      flex: 1,
      backgroundColor: '#7CA1B4',
      alignItems: 'flex-end',
      justifyContent: 'center',
      
  },
  square: {
    backgroundColor: '#7cb48f',
    width: 100,
    height: 100,
    margin: 4,
  },
});

