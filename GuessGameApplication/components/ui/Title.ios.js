import {Text, StyleSheet, Platform} from 'react-native'

function Title ({children}){
    return (
        <Text style = {styles.title}> {children}</Text>
    )
}
export default Title;

const styles = StyleSheet.create({
    title:{
        fontFamily: 'open-sans-bold',
        // borderWidth: Platform.OS === 'android' ? 2 : 0,
        // borderWidth: Platform.select({ios:0, android: 2}),
        // borderColor: '#fff',
        // borderRadius: 2,
        textAlign: 'center',
        // fontWeight: 'bold',
        fontSize: 24,
        color:'#fff',
        padding:12,
        maxWidth: '80%',
        width: 300
    }
})