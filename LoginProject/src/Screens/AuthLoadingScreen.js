import React from "react";
import { ActivityIndicator } from "react-native";
import Background from "../components/Background";
import firebase from "firebase/compat";

export default function AuthLoadingScreen({navigation}){
    firebase.auth().onAuthStateChanged((user) => {
    if (user) {
        navigation.reset({
            routes: [{name: "HomeScreen"}],
        });

    }
    else {
        navigation.reset({
            routes: [{ name: "StartScreen"}],
        });
    }
});
return (
    <Background>
        <ActivityIndicator size= {"large"} />
    </Background>
)
}
