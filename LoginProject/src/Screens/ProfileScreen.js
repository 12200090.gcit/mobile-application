import React from "react";
import Header from "../components/Header";
import Background from "../components/Background";

function NotificationsScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button onPress={() => navigation.goBack()} title="Go back home" />
    </View>
  );
}

const ProfileScreen = () => {
    return (
      <Background>
          <Header>Profile</Header>
      </Background>
    )
}

export default ProfileScreen;