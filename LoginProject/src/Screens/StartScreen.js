import React from 'react';
import { View, StyleSheet } from 'react-native';
import Header from '../components/Header';
import Paragraph from '../components/Paragraph';
import Button from '../components/Button';
import Background from '../components/Background';
import Logo from '../components/Logo';

const StartScreen = ({navigation}) => {
  return(
    <Background>
      <Logo />
      <Header>Login Template</Header>
      <Paragraph>The Easiest Way to start with your amazing application.</Paragraph>
      <Button 
        mode = "outlined" 
        onPress = {() => {
            navigation.navigate("LoginScreen")
        }}> Login</Button>
      <Button 
        mode = "contained"
        onPress = {() => {
            navigation.navigate("RegisterScreen")
        }}
        > Sign Up</Button>
    </Background>
  );
}

export default StartScreen;
