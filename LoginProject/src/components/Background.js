import React from "react";
import { View, StyleSheet, KeyboardAvoidingView } from 'react-native';
import { theme } from "../core/theme";

export default function Background({children}) {
  return(
    <View style={StyleSheet.background}>
      <KeyboardAvoidingView style={styles.container} behavior="padding">
          {children}
      </KeyboardAvoidingView>
    </View>
  )
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        width: "100%",
        backgroundColor: theme.colors.tint,
    },
    container: {
        padding: 20,
        marginTop: 100,
        width: "100%",
        maxWidth: 340,
        alignSelf: "center",
        alignItems: "center",
        justifyContent: "center",
    },
});