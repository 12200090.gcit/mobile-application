// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
export const firebaseConfig = {
  apiKey: "AIzaSyCafHqZu82GjyNjdb2E8yqzIEOvzALcsZ0",
  authDomain: "my-app-e4da7.firebaseapp.com",
  projectId: "my-app-e4da7",
  storageBucket: "my-app-e4da7.appspot.com",
  messagingSenderId: "680533468813",
  appId: "1:680533468813:web:ac829ddc0b6b38f342e4c4"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);