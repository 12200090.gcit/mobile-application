import React from "react";
import { View, StyleSheet,Text } from "react-native";

const MyComponent = () => {
    const greeting = 'Tenzin Tshomo';
    const greeting1 = <Text> Ziggy</Text>
    return (
        <View>
            <Text style = {styles.textStyle}>This is a demo of JSX.</Text>
            <Text>Hi there!!!{greeting}</Text>
            {greeting1}
        </View>
    )
};

export default MyComponent;

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 24
    }

})