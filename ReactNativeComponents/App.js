import React from 'react';
import {View} from 'react-native';
import ViewComponent from './components/ViewComponents';
import TextComponent from './components/text';
import ImageComponent from './components/image';
import TextInputComponent from './components/TextInput';

export default function App() {
  return (
    <View>
     <ViewComponent></ViewComponent>
     <TextComponent></TextComponent>
     <ImageComponent></ImageComponent>
     <TextInputComponent></TextInputComponent>
    </View>
  );
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
