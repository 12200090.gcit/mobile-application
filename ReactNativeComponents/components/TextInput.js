import React, {useState} from 'react';
import {TextInput, View} from 'react-native';

const TextInputComponent = () => {
    const [text, setText] = useState('');
    return (
        <View
            style= {{padding: 100, backgroundColor: text }}>
                {/* <TextInput
                style={{  height: 50, width:20, borderColor: 'yellow', borderwidth: 3, marginBottom: 20}}
                placeholder = 'Enter the text here'
                
                multiline={false}
                onChangeText={text => setText(text.toLowerCase())}>
                </TextInput> */}
                <TextInput
                    style={{ height: 40, width: "95%", borderColor: 'yellow', borderWidth: 1,  marginBottom: 20 }}
          // Adding hint in TextInput using Placeholder option.
                    placeholder="Enter the text here"
                    multiline={false}
                    onchangeText = {text => setText(text.toLowerCase())}
                />

          </View>
    );
}

export default TextInputComponent;