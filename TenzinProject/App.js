import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { add, multiply } from './Component/nameExport';
import division from './Component/defaultExport';
import Courses from './Component/funcComponent';

export default class App extends React.Component {
  render = () => (
    <View style={styles.container}>
      <Text>Named export and default export.</Text>
      <Text>Hello tenzin tshomo!</Text>
      <Text>Result of Addition: {add(5,6)}</Text>
      <Text>Result of Multiplication: {multiply(5,8)}</Text>
      <Text>Result of Division: {division(10,2)}</Text>
      
      <View style={{margin: '10%'}}></View>
      
      <Text>Practical 2</Text>
      <Text>Stateless and statefull components</Text>
      <Text style ={styles.text}>
          I m now ready to start the journey.
      </Text>

      <View style={{margin: '10%'}}></View>

      <Courses></Courses>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    padding: '10%',
    justifyContent: 'center',
  },
  text: {
    marginTop: '5%'
  }
});
