import React from 'react';
import { SafeAreaView, StyleSheet, TextInput, Text } from 'react-native';

const App = () => {
  const [name, setName] = React.useState('');
  const [pressedEnterKey, setPressedEnterKey] = React.useState(false);

  const greetUser = (e) => {
    
    if(e.nativeEvent.key == "Enter") {
       setPressedEnterKey(true);
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      {pressedEnterKey == true ? (
        <>
        <Text style={styles.pl_1}>
          Hi {name} from Gyalpozhing College of Information Technlogy.
        </Text>
        <TextInput
            style={styles.input}
            value={"***********************************"}
          />
        </>
      ) : (
        <>
          <Text style={styles.pl_1}>What is your name?</Text>
          <TextInput
            style={styles.input}
            onChangeText={setName}
            value={name}
            onKeyPress={ (e) => greetUser(e)}
          />
        </>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    backgroundColor: '#ebebeb',
    borderColor: '#ebebeb',
  },
  pl_1: {
    paddingLeft: 10,
  },
});

export default App;
