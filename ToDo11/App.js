import React, { useState } from 'react';
import { Text, View, StyleSheet, Button, Pressable } from 'react-native';

const App = () => {
  const [count, setCount] = useState(0);

  return (
    <View style={styles.container}>
      <Text style={styles.paragraph}>
        {count == 0
          ? `The button isn't pressed yet`
          : `The button was pressed ${count} times!`}
      </Text>
      <Pressable
        onPress={() => setCount(count + 1)}
        disabled={count > 2 ? true : false}
        style={count > 2 ? styles.disabled : styles.btn}>
        <Text style={styles.text}>PRESS ME</Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  paragraph: {
    fontSize: 18,
    textAlign: 'center',
  },
  btn: {
    backgroundColor: 'blue',
    padding: 10,
    textAlign: 'center',
  },
  text: {
    color: 'white',
  },
  disabled: {
    backgroundColor: 'gray',
    color: 'white',
    padding: 10,
    textAlign: 'center',
  },
});

export default App;
