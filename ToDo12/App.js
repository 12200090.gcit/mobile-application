import React, { useState } from 'react';
import {
  View,
  Text,
  TextInput,
  Pressable,
  FlatList,
  StyleSheet,
} from 'react-native';

const App = () => {
  const [text, setText] = useState('');
  const [goals, setGoal] = useState([]);

  const handleGoalInput = () => {
    setGoal([...goals, text]);
    setText('');
  };

  const handleDelete = (e) => {
    console.log(e)
  }

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder="Course Goal"
        value={text}
        onChangeText={setText}
      />

      <View style={styles.buttonGroup}>
        <Pressable onPress={() => setText('')}>
          <Text style={styles.cancel}>CANCEL</Text>
        </Pressable>
        <Pressable onPress={() => handleGoalInput()}>
          <Text style={styles.add}>ADD</Text>
        </Pressable>
      </View>

      <View>
        {goals.map(goal => (
          <Text style={styles.list}>
            {goal}
          </Text>
        ))}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    borderColor: '#ccc',
    borderWidth: 1,
    padding: 8,
    width: '80%',
  },
  buttonGroup: {
    flexDirection: 'row',
    marginTop: 10,
  },
  cancel: {
    color: '#ff0000',
    fontWeight: 'bold',
    padding: 5,
  },
  add: {
    color: '#0000ff',
    fontWeight: 'bold',
    padding: 5,
  },
  list: {
    justifyContent: 'flex-start',
    padding: 2,
    marginLeft: 0,
  },
});

export default App;