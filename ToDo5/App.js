import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  var name = "Tenzin Tshomo";
  return (
    <View style={styles.container}>
      <Text style ={{fontSize: 45, textAlign: 'center'}}>Getting started with react native!</Text>
      <Text style ={{fontSize: 20,textAlign: 'center'}}> My name is {name}</Text>
      <StatusBar style="auto" />
    </View>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: 200,
  
    
  },
});
