import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default function App() {
  return (
   
      <View style={styles.wrapper}>
        <View style={styles.box1}>
          <Text>1</Text>
        </View>
        <View style={styles.box2}>
          <Text>2</Text>
        </View>
        <View style={styles.box3}>
          <Text>3</Text>
        </View>
      </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    marginTop: 50,
    flexDirection: 'row',
    marginLeft: 20,
    marginRight: 50,
    height: 150
  },
  box1: {
    flex: 29,
    backgroundColor: "red",
    justifyContent: "center",
    alignItems: "center"
  },
  box2: {
    flex:70,
    backgroundColor: "blue",
    justifyContent: "center",
    alignItems: "center"
  },
  box3: {
    flex: 5,
    backgroundColor: "green",
    justifyContent: "center",
    alignItems: "center"
  }
});