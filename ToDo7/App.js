import React from 'react';
import { View, StyleSheet } from 'react-native';


export default function App() {
  return (
    <View style={styles.container}>
     <View style={ { ...styles.square, ...styles.red } } ></View>
     <View style={ { ...styles.square, ...styles.blue } } ></View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 8,
    alignItems: "center"
  },
  square: {
    width: 100,
    height: 100
  },
  red: {
    backgroundColor: "#ff0000"
  },
  blue: {
    backgroundColor: "#0000ff"
  }
});