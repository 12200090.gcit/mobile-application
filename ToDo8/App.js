import React from 'react';
import { View, StyleSheet, Text } from 'react-native';


export default function App() {
  return (
    <View style={styles.container}>
      <Text style={styles.t_16}>
        A 
        <Text style={styles.bold}> quick brown fox </Text>
        jumps over the lazy dog.
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 8
  },
  t_16: {
    fontSize: 16
  },
  bold: {
    fontWeight: 'bold'
  }
});