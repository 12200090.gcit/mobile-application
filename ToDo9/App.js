import React from 'react';
import { View, StyleSheet, Image } from 'react-native';


export default function App() {
  return (
    <View style={styles.container}>
     <Image style={styles.logo} source={{uri: "https://picsum.photos/100/100"}} />
     <Image style={styles.logo} source={require('./assets/react-native.png')} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 8,
    alignItems: "center"
  },
  logo: {
    height: 100,
    width: 100,
    margin: 5
  }
});