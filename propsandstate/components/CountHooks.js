import React, { useState } from 'react';
import { Text, View, Button} from 'react-native';
const CountHook = () => {
    const [count, setCount] = useState(0);
    return (
        <View>
            <Text>
                I clicked { count } times
            </Text>
            <Button
            onPress = { () => setCount(count + 1) }
            title = "Click me!"
            />
        </View>
    );
};
export default CountHook;